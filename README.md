 NOTE This README.md file should be placed at the root of each of your main directory.

# LIS4381 Mobile Web App Dev

## Conner Bradley

### Class Number Requirements

Course Work Links

* [A1 README.md](a1/README.md "My a1 README.md file")
    1. Install AMPPS
    1. Install JDK
    1. Install Andriod Studio and create my First App
    1. Provide screenshots of installations
    1. Create bitbucket repo
    1. Complete bitbucket tutorials (bitbucketstationlocations and myteamquotes)
    1. Provide git command descriptions
* [A2 README.md](a2/README.md "My a2 README.md file")
    1. Create a mobile recipe app using Android Studio.
    1. Include screenshots of app
    1. Ch 3-4 questions
    1. Include screenshots of skillset 1 Even Or Odd Calculator
    1. Include screenshots of skillset 2 Largest Number Calculator
    1. Include screenshots of skillset 3 Array Loops Examples
* [A3 README.md](a3/README.md "My a3 README.md file")
    1. create ERD based upon business rules
    1. provide screenshot of completed ERD
    1. provide DB resource links
* [A4 README.md](a4/README.md "My a4 README.md file")
    1. Design your mobile-first web application's splash page.
    1. Code data entry forms with basic client-side validation using regular expressions and pass/fail tests into your web application. 
    1. Develop separate programs to do the following:
        * SS10 - Determines if a user-entered value is alpha, numeric, or special.
        * SS11 - Populates ArrayList of stringfs with user-entered animal type values.
        * SS12 - Convers user-entered temperature into Fahrenheit or Celsius scales. Continues to prompt user until told not to.
    1. Develop README.md in markdownload language and push to repository when finished. 
    1. Display screencaptures of your results.
    1. Review the chapter questions for PHP/MySQL chapters 9 - 10, 15.  
* [A5 README.md](a5/README.md "My a5 README.md file")

    1. Code data entry forms with basic client-side, and server-side validation using regular expressions and pass/fail tests into your web application. Also use placeholders.
    1. Develop separate programs to do the following:
        * SS13 - Write a program to calculate the volume of sphere.
        * SS14 - Build a simple calculator in your webapp.
        * SS15 - Build a text entry form to read user-entered text into a txt file.
    1. Develop README.md in markdownload language and push to repository when finished. 
    1. Display screencaptures of your results.
    1. Review the chapter questions for PHP/MySQL chapters 11 - 12, 19. 

* [P1 README.md](p1/README.md "My p1 README.md file")
    1.  Create a Mobile App Business Card
    1. Include the following:
        * Text Shadow
        * Borders around image and button
        * Background color
        * Display launcher icon on both activities
    1. Skillsets 7-9
    1. Chapter questions (Ch9,10) 
* [P2 README.md](p2/README.md "My p2 README.md file")
    1. Code edit and delete buttons into A5's functionality. 
    1. Use Cross Side Scripting (XSS) and prepared statements to help prevent SQL injection. 
    1. Develop README.md in markdownload language and push to repository when finished. 
    1. Display screencaptures of your results.
    1. Review the chapter questions for PHP/MySQL. 
