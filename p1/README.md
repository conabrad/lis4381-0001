**NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 Mobile Web App Development

## Conner Bradley

### Project 1 Requirements:

1. Create a Mobile App Business Card
2. Include the following:
    * Text Shadow
    * Borders around image and button
    * Background color
    * Display launcher icon on both activities
3. Skillsets 7-9
4. Chapter questions (Ch9,10)

#### README.md file should include the following items:

* Screenshots of both UIs
* Screenshots of skillsets:
    * Q7_rand_num_generator_data_vaild.png
    * Q8_largest_three.png
    * Q9_array_runtime_data_validation.png


> 
#### Project Screenshots:

| *Screenshot of Splash Screen*: | *Screenshot of Details* |
|:-----------------------:|:------------------------------:|
| ![Business Card Splash Screenshot](img/splash_ui.png "Splash screen") | ![Details Screenshot](img/details_ui.png "Details UI") |

| *Screenshot of Skillset 7: Random Number Generator with Data Validation*: | *Screenshot of Skillset 8: Largest of Three*: | *Screenshot of Skillset 9: Array Runtime Data Validation*: |
|:-------------------------------------------:|:-------------------------------------------:|:------------------------------------------------:|
[![Q7_rand_num_gen_data_valid Screenshot](img/Q7_rand_num_gen_data_valid.png)](img/Q7_rand_num_gen_data_valid.png) | [![Q8_largest_three Screenshot](img/Q8_largest_three.png)](img/Q8_largest_three.png) | [![Q9_array_runtime_data_validation Screenshot](img/Q9_array_runtime_data_validation.png)](img/Q9_array_runtime_data_validation.png)
