**NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Mobile App Dev

## Conner Bradley

### Assignment #2 Requirements:

*Sub-Heading:*

1. Create a mobile recipe app using Android Studio.
2. Include screenshots of app
3. Ch 3-4 questions
4. Include screenshots of skillsets 1-3

#### README.md file should include the following items:

* screenshot of running application's first UI
* screenshot of running app's second UI
* Git commands
* BitBucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstation locations and myteamquotes)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - one way to start a new project with Git
2. git status - shows status of working tree
3. git add - add files to staging area
4. git commit -m "message" - creates and saves snapshot of changes
5. git push - send local commits to the master branch of the remote repo
6. git pull - merges all changes in the remote repo with the local pwd
7. git remote -v - views all remote repos

#### Assignment Screenshots:

| *Screenshot of running application's first UI*: | *Screenshot of running application's second UI*: |
|:-----------------------------------------------:|:-------------------------------------------------|
![Screenshot of running application's first UI](img/first_ui.png) | ![Screenshot of running application's second UI](img/second_ui.png)

*Screenshot of Skillset Q1_even_or_odd*:

![Screenshot of Skillset Q1](img/Q1_even_or_odd.png)

*Screenshot of Skillset Q2_largest_number*:

![Screenshot of Skillset Q2](img/Q2_largest_number.png)

*Screenshot of Skillset Q3_arrays_loops*:

![Screenshot of Skillset Q3](img/Q3_arrays_loops.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")