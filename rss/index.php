<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
	<link rel="icon" href="../favicon.ico">
	<title>Using RSS Feeds</title>
</head>
<body>

	<?php 
	
	//note: RSS specification: https"//validator.w3.org/feed/docs/rss2.html

	$html ="";
	$publisher = "THE BIBLE IN A YEAR with Fr. Mike Schmitz";
	$url = "https://feeds.fireside.fm/bibleinayear/rss";
	//http://rss.cnn.com/rss/cnn_latest.rss doesnt work in this form, need 'feeds.' not rss


	$html .='<h2>' . $publisher . '</h2>';
	$html .= $url;

	$rss = simplexml_load_file($url);
	$count = 0;
	$html .= '<ul>';
	foreach($rss->channel->item as $item)
	{
		$count++;
		if($count > 10)
		{
			break;
		}
		$html .= '<li><a href="'. htmlspecialchars($item->link) .'">' . htmlspecialchars($item->title) . '</a><br />';
		$html .= htmlspecialchars($item->description) . '<br />';
		$html .= htmlspecialchars($item->pubDate) . '</li><br />';
	}
	$html .= '</ul>';
	
	print $html;

	?>



</body>
</html>
