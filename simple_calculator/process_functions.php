<?php
//show errors: at least 1 and 4...
ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(E_ALL);

//use for inital test of form inputs
//exit(print_r($_POST));

//exit(print_r($_POST)); //display $_POST array values from form

// or, for nicer display in browser...
/* echo "<pre>";
 * print_r($_POST);
 * echo "</pre>";
 * exit(); //stop processing, otherwise, errors below
 */

//After testing, comment out above lines.
               
//code to process inserts goes here

//include('index.php'); //forwarding is faster, one trip to server


?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Corbin Haney">
	<link rel="icon" href="../favicon.ico">

	<title>LIS4381 - Skillset 14</title>
		<?php include_once("../css/include_css.php"); ?>
</head>
<body>	
<?php include_once("../global/nav.php"); ?>

<div class="container">
    <div class="starter-template">
                <div class="page-header">
                    <?php include_once("global/header.php"); ?>	
                </div>

                <h2>Perform Calculation</h2>

    <?php
    //use for both non-function and function versions
    if(!empty($_POST))
    {
        $num1 = $_POST['num1'];
        $num2 = $_POST['num2'];
        $operation = $_POST['operation'];

        if (preg_match('/^[-+]?[0-9]*\.?[0-9]+$/', $num1) & preg_match('/^[-+]?[0-9]*\.?[0-9]+$/', $num2)) // make sure the values entered in num1 and num2 forms are actual numbers using regular expression
        {
            echo '<h2>'. "$operation". '</h2>';

            //create functions to do the operation
            function AddNum($x, $y)
            {
                echo "$x" . " + " . "$y" . " = ";
                echo $x + $y;

            }
            function SubtractNum($x, $y)
            {
                echo "$x" . " - " . "$y" . " = ";
                echo $x - $y;
                
            }
            function MultiplyNum($x, $y)
            {
                echo "$x" . " * " . "$y" . " = ";
                echo $x * $y;
                
            }
            function DivideNum($x, $y)
            {
                if($y == 0)
                {
                    echo "Cannot divide by zero!";
                }
                else
                {
                    echo "$x" . " / " . "$y" . " = ";
                    echo $x / $y;
                }
            }
            function PowerNum($x, $y)
            {
                echo "$x" . " raised to the power of " . "$y" . " = ";
                echo pow($x, $y);
            }

            //call functions
            if($operation == 'addition')
            {
                AddNum($num1, $num2);
            }
            else if($operation == 'subtraction')
            {
                SubtractNum($num1, $num2);
            }
            else if($operation == 'multiplication')
            {
                MultiplyNum($num1, $num2);
            }
            else if($operation == 'division')
            {
                DivideNum($num1, $num2);
            }
            else if($operation == 'exponentiation')
            {
                PowerNum($num1, $num2);
            }
        }

    }
    ?>
