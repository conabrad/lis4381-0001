

# LIS 4381

## Corbin Haney

> ### Assignment #4 Requirements:

1. Develop a web based data entry form.
2. Use jQuery client based data validation to avoid SQL injection.
3. Finish skillsets 10-12.

> #### README.md file should include the following items:

* Screenshot of Main Page
* Screenshot of Failed Data Validation
* Screenshot of Passed Data Validation
* Screenshots of Skillsets

> #### Assignment Screenshots:

Main Page                                   |Failed Validation                               |Passed Validation
--------------------------------------------|------------------------------------------------|---------------------------------------
![Main Page ](img/mainPage.png)             |![Failed Validation](img/failed.png)            |![Passed Validation](img/passed.png)


> #### *Assignment Skillsets*:

Simple Calculator                           |Compound Interest Calculator      |Array Copy
--------------------------------------------|----------------------------------|-----------------------------
![skillset 10](img/Q10.png)                 |![skillset 11](img/Q11.png)      |![skillset 12](img/Q12.png)

