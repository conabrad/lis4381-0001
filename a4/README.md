
| Developed by: Conner A. Bradley |
| ------------------------------: |
# Web Application Development Assignment 4 

***
***

### Requirements:

1. Design your mobile-first web application's splash page.
1. Code data entry forms with basic client-side validation using regular expressions and pass/fail tests into your web application. 
1. Develop separate programs to do the following:
    * SS10 - Determines if a user-entered value is alpha, numeric, or special.
    * SS11 - Populates ArrayList of stringfs with user-entered animal type values.
    * SS12 - Convers user-entered temperature into Fahrenheit or Celsius scales. Continues to prompt user until told not to.
1. Develop README.md in markdownload language and push to repository when finished. 
1. Display screencaptures of your results.
1. Review the chapter questions for PHP/MySQL chapters 9 - 10, 15. 


### Results:
>**NOTE:** Click any screencapture to view an enlarged version

| *Splash Screen*: | *Failing Data*: | *Passing Data*: |
|:-----------------------:|:------------------------------:|:-------------------------:|
| [![Online Portfolio Splash Screenshot](img/splash.png "Splash screen")](img/splash.png) | [![Data Validation Fail Screenshot](img/invalid.png "Data validation failure")](img/invalid.png) | [![Data Validation Pass Screenshot](img/valid.png "Data validation pass")](img/valid.png) |

| *Skillset 10: Determining Value*: | *Skillset 11: Array List*: | *Skillset 12: Temperature Conversion*: |
|:-------------------------------------------:|:-------------------------------------------:|:------------------------------------------------:|
[![Q10_alpha_numeric_special Screenshot](img/Q10_alpha_numeric_special.png)](img/Q10_alpha_numeric_special.png) | [![Q11_array_list Screenshot](img/Q11_array_list.png)](img/Q11_array_list.png) | [![Q12_temp_conv Screenshot](img/Q12_temp_conv.png)](img/Q12_temp_conv.png)

### Referenced in README.md:

* project requirements
* splash.png
* invalid.png
* valid.png
* skillsets:
    * Q10_alpha_numeric_special.png
    * Q11_array_list.png
    * Q12_temp_conv.png