
| Developed by: Conner A. Bradley |
| ------------------------------: |
# Web Application Development Project 2

***
***

### Requirements:

1. Code edit and delete buttons into A5's functionality. 
1. Use Cross Side Scripting (XSS) and prepared statements to help prevent SQL injection. 
1. Develop README.md in markdownload language and push to repository when finished. 
1. Display screencaptures of your results.
1. Review the chapter questions for PHP/MySQL. 


### Results:
>**NOTE:** Click any screencapture to view an enlarged version

| *Main Page*: | *index.php*: |
|:-----------------------:|:----------: |
| [![Main Page Screenshot](img/splash.png "Main")](img/splash.png) | [![index.php Screenshot](img/index.png "index.php")](img/index.png) |

| *edit_petstore*: | *Error*: | *Updated Table*: | 
| :--------------------:|:-------------------------:| :-------------: |
| [![edit_petstore Page Screenshot](img/edit_petstore.png "edit petstore")](img/edit_petstore.png) | [![Error Screenshot](img/error.png "error")](img/error.png) | [![Updated Table](img/update.png "Updated data")](img/update.png) |


| *Delete Check*: | *Deleted Record*: |
|:-------------------------------------------:|:-------------------------------------------:|
[![delete_check Screenshot](img/delete_check.png)](img/delete_check.png) | [![delete Screenshot](img/delete.png)](img/delete.png) |

| *RSS Feed*: |
|:-----------:|
[![RSS Screenshot](img/rss.png)](img/rss.png)

### Referenced in README.md:

* project requirements
* splash.png
* index.png
* edit_petstore.png
* error.png
* update.png
* delete_check.png
* delete.png
* rss.png
