
| Developed by: Conner A. Bradley |
| ------------------------------: |
# Web Application Development Assignment 5 

***
***

### Requirements:

1. Code data entry forms with basic client-side, and server-side validation using regular expressions and pass/fail tests into your web application. Also use placeholders.
1. Develop separate programs to do the following:
    * SS13 - Write a program to calculate the volume of sphere.
    * SS14 - Build a simple calculator in your webapp.
    * SS15 - Build a text entry form to read user-entered text into a txt file.
1. Develop README.md in markdownload language and push to repository when finished. 
1. Display screencaptures of your results.
1. Review the chapter questions for PHP/MySQL chapters 11 - 12, 19. 


### Results:
>**NOTE:** Click any screencapture to view an enlarged version

| *Main Table*: | *Failing Data*: | *Server-Side Error*: | *Passing Data*: | *Added Data*: | 
|:-----------------------:|:----------: | :--------------------:|:-------------------------:| :-------------: |
| [![Petstore Table Screenshot](img/main.png "Main")](img/main.png) | [![Data Validation Fail Screenshot](img/invalid.png "Data validation failure")](img/invalid.png) | [![Server-Side Error Screenshot](img/server_side_error.png "Server-side error")](img/server_side_error.png) | [![Data Validation Pass Screenshot](img/valid.png "Data validation pass")](img/valid.png) | [![New Data Screenshot](img/new_record.png "New data")](img/new_record.png) |

| *Skillset 13: Sphere Volume*: | *Skillset 14: Simple Calculatort*: | *Skillset 15: Read / Write Text*: |
|:-------------------------------------------:|:-------------------------------------------:|:------------------------------------------------:|
[![Q13_sphere_volume_calculator Screenshot](img/Q13_sphere_volume_calculator.png)](img/Q13_sphere_volume_calculator.png) | [![Q14_simple_calculator Screenshot](img/Q14_simple_calculator.png)](img/Q14_simple_calculator.png) | [![Q15_read_write Screenshot](img/Q15_read_write.png)](img/Q15_read_write.png)

### Referenced in README.md:

* project requirements
* main.png
* invalid.png
* server_side_error.png
* valid.png
* new_record.png
* skillsets:
    * Q13_sphere_volume_calculator.png
    * Q14_simple_calculator.png
    * Q15_read_write.png