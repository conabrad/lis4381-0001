import java.util.Scanner; 

public class Methods
{
    public static void getRequirements()
    {
        //display operational messages
        System.out.println("Developer: Conner Bradley");
        System.out.println("Program evaluates integers as even or odd.");
        System.out.println("Note: Program does *not* check for non-numeric characters");
    }



    public static void evaluateNumbers()
    {
        //iniialize variables, create Scanner object, capture user input
        int x = 0;
        System.out.print("Enter integer: ");
        Scanner sc = new Scanner(System.in); 
            x = sc.nextInt();
        
        if ( x % 2 == 0 )
        {
            System.out.println( x + " is an even number.");
        }
        else
        {
            System.out.println(x + " is an odd number.");
        }
    }
}