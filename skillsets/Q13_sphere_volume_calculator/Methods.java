import java.util.Scanner;   

public class Methods 
{
    //create Method w/o returning any value (w/o object)
    public static void getRequirements()
    {
        System.out.println("Sphere Volume Program" + "\nDeveloper: Conner Bradley\n");
        System.out.println("\nProgram calcuates sphere volume in liquid U.S. gallons from user-entered diameter value in inches,");
        System.out.println("and rounds to two decimal places.");
        System.out.println("Must use Java's built in PI and pow() capabilities.");
        System.out.println("Program checks for non-ints and non-numeric values." + 
        "Program continues to prompt for user entry until no longer requested, " + 
        "prompt accepts upper or lower case letters.");
    }
    
    public static void getSphereVolume()
    {
        Scanner sc = new Scanner(System.in);
        int diameter = 0;
        double volume = 0.0;
        double gallons = 0.0;
        char choice = ' ';

        //note: do...while always executes at least once

        do
        {
            System.out.print("\nPlease enter diameter in inches: ");
            while (!sc.hasNextInt())
            {
                System.out.println("Not a vlid integer!\n");
                sc.next();//prevents inf loop
                System.out.print("Please try again. Enter diameter in inches: ");
            }
            diameter = sc.nextInt();

            System.out.println();

            //Note: must use floating points values other int division 
            volume = ((4.0/3.0) * Math.PI * Math.pow(diameter/2.0, 3)); //output in cubic inch
            gallons = volume/231; //concert cubic inch to gals
            System.out.println("Sphere volume: " + String.format("%,.2f", gallons) + " liquid U.S. gallons");

            System.out.print("\nDo you want to calculate another sphere volume (y or n)? ");
            choice = sc.next().charAt(0);
            choice = Character.toLowerCase(choice);
        }
        while(choice == 'y');

        System.out.println("\nThank you for using our Sphere Volume Calculator!");
        sc.close();
    }
}


