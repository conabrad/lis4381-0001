
import java.util.Scanner;   


public class Methods 
{
    //create Method w/o returning any value (w/o object)
    public static void getRequirements()
    {
        System.out.println("Developer: Conner Bradley");
        System.out.println("Program evaluates user-entered characters.");
        System.out.println("Use the following chars: W or w, C or c, H or h, N or n.");
        System.out.println("Use follwoing decision structures: is...else, and switch");
        System.out.println();
    }
    
    public static void phoneType()
    {
        String userChoice = "";
        char userPhone = ' ';
        Scanner sc = new Scanner(System.in);

        //no way to get chars from scanner, must do this string to char method
        System.out.println("Phone types: W or w (work), C or c (cell), H or h (home), N or n (none).");
        System.out.print("Enter phone type: ");
        userChoice = sc.next().toLowerCase();
        userPhone = userChoice.charAt(0);

        //if...else decision struct
        System.out.println("\n\nif...else:");
        if (userPhone == 'w')
        {
            System.out.println("Phone type: work");
        }
        else if (userPhone == 'c')
        {
            System.out.println("Phone type: cell");
        }
        else if (userPhone == 'h')
        {
            System.out.println("Phone type: home");
        }
        else if (userPhone == 'n')
        {
            System.out.println("Phone type: none");
        }
        else 
        {
            System.out.println("Incorrect character entry.");
        }

        //switch decision struct
        System.out.println("\n\nswitch:");

        switch(userPhone)
        {
            case 'w':
                System.out.println("Phone type: work");
                break;
            case 'c':
                System.out.println("Phone type: cell");
                break;
            case 'h':
                System.out.println("Phone type: home");
                break;
            case 'n':
                System.out.println("Phone type: none");
                break;
            default: 
                System.out.println("Incorrect character entry.");
                break;
        }

    }
}
