import java.util.Scanner; 
import java.util.Random;  

public class Methods 
{
    //create Method w/o returning any value (w/o object)
    public static void getRequirements()
    {
        System.out.println("Developer: Conner Bradley");
        System.out.println("Print min and max int values");
        System.out.println("Program prompts user to enter desired number of  pseudorandom-generated ints (min 1)");
        System.out.println("Use follwoing loop structures: for, enchanced for, while, do...while.");

        //print min/max int values
        System.out.println("Integer.MIN_VALUE = " + Integer.MIN_VALUE);
        System.out.println("Integer.MAX_VALUE = " + Integer.MAX_VALUE);

        System.out.println();
        return; //OK, however not necessary in void method

    }

    public static int[] createArray()
    {
        Scanner sc = new Scanner(System.in);
        int arraySize = 0;

        //prompt user for number of randomly generated numbers
        System.out.print("Enter desired number of pseudorandom integers (min 1): ");
    
        arraySize = sc.nextInt();

        int yourArray[] = new int[arraySize];
        return yourArray;
    }
    
    public static void generateRandNums(int [] myArray)
    {
        
        Random rand = new Random(); //instantiate random object variable
       
        //create loops to randomize integer values
        int i = 0;//initialize counter variable

        System.out.println("for loop: ");
        for (i = 0; i < myArray.length; i++)
        {
            System.out.println(rand.nextInt());
        }
        
        
        System.out.println("\nenhanced for loop: ");
        for (int n: myArray)
        {
            System.out.println(rand.nextInt());
        }
        
        System.out.println("\nwhile loop: ");
        i = 0;
        while (i < myArray.length)
        {
            System.out.println(rand.nextInt());
            i++;
        }
        
        i=0;//reassign to zero
        System.out.println("\ndo...while loop: ");
        do
        {
            System.out.println(rand.nextInt());
            i++;
        }
        while (i < myArray.length);
        
    }
}
