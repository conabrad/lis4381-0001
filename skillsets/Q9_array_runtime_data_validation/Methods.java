import java.util.Scanner; 

/*
Arrays:
1. container objects that hold a fixed number of values of a single datat type (int, double, String)
2. Length of an array is established upon creation--after creation, its length is fixed

Array vs ArrayList:
1. resizeable:
    array: static (fixed) data structure can not change
    ArrayList: dynamic
2. Data types:
    Array: contain both primitive types(int, float, double) as well as objects
    ArrayList: can *not* contain primitive data types. only objects
3. Access and Modify Data:
    Array: members accessed using []
    ArrayList: set of methods to access elements and modify them (add(), remove(),etc)
4. Multi-dimensional:
    Array: can be multi-dimension
    ArrayList: only single dim
*/

public class Methods 
{   
    //create global scanner object, used in more than one method
    //Note: using "final" prevents object variable from being modified
    static final Scanner sc = new Scanner(System.in);
    
    //nonvalue returning w/o returning any value (w/o object)
    public static void getRequirements()
    {
        System.out.println("Developer: Conner Bradley");
        System.out.println("1) Program creates array size at run-time.");
        System.out.println("2) Program display array size.");
        System.out.println("3) Program rounds sum and average of numbers to two decimal places.\n4) Numbers *must* be float data type, not double.");

        System.out.println();
        return; //OK, however not necessary in void method

    }

    //value returning method
    public static int validateArraySize()
    {
        int arraySize = 0;
        System.out.print("Enter array size: ");
    
        while(!sc.hasNextInt())
        {
            System.out.println("Not valid integer!\n");
            sc.next();//if ommitted, inf loop
            System.out.print("Please try again! Enter array size: ");
        }
        arraySize = sc.nextInt();
        System.out.println();
        return arraySize;
    
    }

    //nonvalue-returning method (static requires no object)
    public static void calculateNums(int arraySize)
    {
        float sum = 0.0f;
        float average = 0.0F;

        //indicate number of values required, based upon user input
        System.out.print("Please enter: " + arraySize + " numbers.\n");

        //create array for stroing user input, based upon user-entered array size
        float numsArray[] = new float[arraySize];

        //validate data entry 
        for(int i = 0; i < arraySize; i++)
        {
            System.out.print("Enter number " + (i+1) + ": ");
            
            while(!sc.hasNextFloat())
            {
                System.out.println("Not valid integer!\n");
                sc.next();//if ommitted, inf loop
                System.out.print("Enter number " + (i+1) + ": ");
            }
            numsArray[i] = sc.nextFloat(); //capture validated user input
            sum = sum + numsArray[i]; //process data entry 
        }
        average=sum/arraySize;

        //print nums entered
        System.out.print("\nNumbers entered: ");
        for (int i=0; i < numsArray.length; i++)
            System.out.print(numsArray[i] + " ");

        //call method to print and format numbers
        printNums(sum, average);

    }
    
    //nonvalue
    public static void printNums(float sum, float average)
    {
           System.out.println("\nSum: " + String.format("%.2f", sum));
           System.out.println("Average: " + String.format("%.2f", average));     
    }
}
