import java.util.Arrays;

class Main 
{
    public static void main(String arg[]) 
    {
        //call static void methods (i.e, no object, non-value returning)
        Methods.getRequirements();

        int arraySize = 0;
        arraySize = Methods.validateArraySize();

        Methods.calculateNums(arraySize);
    }   
}
