import java.util.Scanner;

public class Methods 
{
    //create Method w/o returning any value (w/o object)
    public static void getRequirements()
    {
        System.out.println("Developer: Conner Bradley");
        System.out.println("Program determines whether user-entered value is alpha, numeric, or special character.");
        System.out.println("\nReferences:" + "\nASCII Background: https://en.wikipedia.org/wiki/ASCII");
        System.out.println("ASCII Character Table: https://www.ascii-code.com/");
        System.out.println("Lookup Tables: https://www.lookuptables.com/");
    }
    
    public static void determineChar()
    {
        Scanner sc = new Scanner(System.in);

        System.out.print("\nEnter character: ");
        //next() function returns next token
        //Token: smallest elemetn of a program
        //generally, identifiers, keywords, literals operators and punctuation
        //note: white space and comments not tokens, though, sperarate tokens
        //example: "I like this" ("I" is first token, like is second, this is third)
        char ch = sc.next().charAt(0);//capture first char from first token

        //note: can be resolved in different ways
        //here: works with legacy java no need for speciatl functions...
        //test for alpha chars
        if((ch >= 'a' && ch <='z') || (ch >= 'A' && ch <='Z'))
            {
                System.out.println(ch + " is alpha. ASCII value: " + (int) ch);
            }
        else if(ch >= '0' && ch <= '9')
            {
                System.out.println(ch + " is numeric. ASCII value: " + (int) ch);
            }
        else 
            {
                System.out.println(ch + " is a special character. ASCII value: " + (int) ch);
            }

            sc.close();
    }
}
