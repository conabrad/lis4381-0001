import java.util.ArrayList;
import java.util.Scanner;   

public class Methods 
{
    //create Method w/o returning any value (w/o object)
    public static void getRequirements()
    {
        System.out.println("Developer: Conner Bradley");
        System.out.println("Program populates ArrayList of strings with user-entered animal type values.");
        System.out.println("Examples: Polar bear, Guinea pig, dog, cat, bird.");
        System.out.println("Program continues to collect user-entered values until user types \"n\"");
        System.out.println("Program displays ArrayList values after each iteration, as well as size.\n");
    }
    
    public static void createArrayList()
    {
        //create program variable/object
        //create scanner obj
        Scanner sc = new Scanner(System.in);
        ArrayList<String> obj = new ArrayList<String>(); //create string type arraylist
        String myStr = "";
        String choice = "y";
        int num = 0;

        while (choice.equals("y"))
            {
                System.out.print("Enter animal type: ");
                myStr = sc.nextLine();
                obj.add(myStr);//add string object
                num=obj.size(); //returns ArrayList size 
                System.out.println("ArrayList elements:" + obj + "\nArrayList Size = " + num);
                System.out.print("\nContinue? Enter y or n: ");//note: could tyep any char other than y
                choice = sc.next().toLowerCase(); //permits y or Y
                sc.nextLine(); //must use nextLine() to capture EOL char, otherwise inf loop
            }
    }
}
