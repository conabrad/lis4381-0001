import java.util.Scanner;   

public class Methods 
{
    //create Method w/o returning any value (w/o object)
    public static void getRequirements()
    {
        System.out.println("Developer: Conner Bradley");
        System.out.println("Program evaluates the largest of three integers.");
        System.out.println("Note: Program checks for integers and non-numerics");
        System.out.println();
    }
    
    public static void validInput()
    {
        //Declare scanner objects and variables
        Scanner sc = new Scanner(System.in);
        int inp1 = 0, inp2 = 0, inp3 = 0;

        //prompt user for three integers
        System.out.print("Please enter first number: ");
        while(!sc.hasNextInt())
        {
            System.out.println("Not valid integer!\n");
            sc.next();//if ommitted, inf loop
            System.out.print("Please try again! Enter first integer: ");
        }
        inp1 = sc.nextInt();

        System.out.print("Please enter second number: ");
        while(!sc.hasNextInt())
        {
            System.out.println("Not valid integer!\n");
            sc.next();//if ommitted, inf loop
            System.out.print("Please try again! Enter second integer: ");
        }
        inp2 = sc.nextInt();

        System.out.print("Please enter third number: ");
        while(!sc.hasNextInt())
        {
            System.out.println("Not valid integer!\n");
            sc.next();//if ommitted, inf loop
            System.out.print("Please try again! Enter third integer: ");
        }
        inp3 = sc.nextInt();

        System.out.println();

        Methods.getLargestNum(inp1, inp2, inp3);

    }

    public static void getLargestNum(int num1, int num2, int num3)
    {
        if(num1>num2 && num1>num3)
            System.out.println("The first integer: " + num1 + " is the largest.");
            
        else if(num2>num1 && num2>num3)
            System.out.println("The second integer: " + num2 + " is the largest.");
        
        else
            System.out.println("The third integer: " + num3 + " is the largest.");
    }

}
