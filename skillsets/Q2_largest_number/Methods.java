import java.util.Scanner; 

public class Methods
{
    public static void getRequirements()
    {
        //display operational messages
        System.out.println("Developer: Conner Bradley");
        System.out.println("Program evaluates largest of two integers.");
        System.out.println("Note: Program does *not* check for non-numeric characters or non-integer valeus.");
        
    }



    public static void evaluateNumbers()
    {
        //iniialize variables, create Scanner object, capture user input
        int num1, num2;
        Scanner sc = new Scanner(System.in); 

        System.out.print("\nEnter first integer: ");
            num1 = sc.nextInt();

        System.out.print("Enter second integer: ");
            num2 = sc.nextInt();
        
        if ( num1 > num2 )
        {
            System.out.println("The first integer, " + num1 + ", is larger than the second integer: " + num2 );
        }
        else if ( num2 > num1 )
        {
            System.out.println( "The second integer, " + num2 + ", is largest than the first integer: " + num1 );
        }
        else
        {
            System.out.println("Integers are equal.");
        }
    }
}