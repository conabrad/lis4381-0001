public class Methods 
{
    //create Method w/o returning any value (w/o object)
    public static void getRequirements()
    {
        System.out.println("Developer: Conner Bradley");
        System.out.println("Program loops through array of floats.");
        System.out.println("Use the following values: dog, cat, bird, fish, insect");
        System.out.println("Use follwoing loop structures: for, enchanced for, while, do...while.");
        System.out.println();
    }
    
    public static void arrayIteration()
    {
        String animals[] = {"dog","cat","bird","fish","insect"};

        System.out.println("for loop: ");
        for (int i = 0; i < animals.length; i++)
        {
            System.out.println(animals[i]);
        }

        System.out.println("\nenhanced for loop: ");
        for (String test : animals)
        {
            System.out.println(test);
        }
        
        System.out.println("\nwhile loop: ");
        int i=0;
        while (i < animals.length)
        {
            System.out.println(animals[i]);
            i++;
        }
        
        i=0;
        System.out.println("\ndo...while loop: ");
        do
        {
            System.out.println(animals[i]);
            i++;
        }
        while (i < animals.length);
    }
}
