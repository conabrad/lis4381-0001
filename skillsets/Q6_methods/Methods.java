import java.util.Scanner;   

public class Methods 
{
    //create Method w/o returning any value (w/o object)
    public static void getRequirements()
    {
        System.out.println("Developer: Conner Bradley");
        System.out.println("Program prompts user for first name and age, then prints results.");
        System.out.println("Create four methods from the following requirements:");
        System.out.println("1) getRequirements(): Void method displays program requirements.");
        System.out.println("2) getUserInput(): Void method prompts for user input,");
        System.out.println("\tthen calls two methods: myVoidMethod() and myValueReturningMethod().");
        System.out.println("3) myVoidMethod():");
        System.out.println("\ta. Accepts two arguments: String and int.");
        System.out.println("\tb. Prints user's first name and age");
        System.out.println("4) myValueReturningMethod():");
        System.out.println("\ta. Accepts two arguments: String and int.");
        System.out.println("\tb. Returns String containing first name and age.");
        System.out.println();
    }
    
    public static void getUserInput()
    {
        //initialize vars
        String fName = "";
        int age = 0;
        String nameAge = "";
        
        //instantiate scanner
        Scanner scan = new Scanner(System.in);

        System.out.print("\nEnter first name: ");
        fName = scan.nextLine();
        System.out.print("\nEnter age: ");
        age = scan.nextInt();

        System.out.println();

        myVoidMethod(fName,age);
        nameAge = myValueReturningMethod(fName,age);

        System.out.print(nameAge);
    }

    public static void myVoidMethod(String name, int yearsOld)
    {
        System.out.println("\nvoid method call: " + name + " is " + yearsOld);
    }

    public static String myValueReturningMethod(String userName, int yearsOld)
    {
        System.out.print("value-returning method call: ");
        //note: int string conversion of yearsOld (int)
        return userName + " is " + yearsOld;
    }
}
