
class Main 
{
    public static void main(String arg[]) 
    {
        //call static void methods (i.e, no object, non-value returning)
        Methods.getRequirements();

        int[] userArray = Methods.createArray();//java style array

        Methods.generateRandNums(userArray); //pass array
    }   
}
