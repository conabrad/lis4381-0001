**NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Web Application Development

## Conner Bradley

### Assignment #3 Requirements:

*Deliverables:*

1. Entity Relationship Diagram (ERD)
2. FWD Engineer with data (min: 10 records in each table)
3. Provide BitBucket read-only access to repor (Language SQL). *must* include README.md, using Markdown Syntax. Include links to the following:
    * docs folder: a3.sql, a3.mwb
    * img folder: a3.png, skillsets, ERD table records
        * Q4_decision_structure.png
        * Q5_random_number_generator.png
        * Q6_methods.png
    * README.md (must display all pngs)

#### README.md file should include the following items:

* Include links to the following:
    * docs folder: a3.sql, a3.mwb
    * img folder: a3.png, ERD table records
* Display skillsets
    * Q4_decision_structure.png
    * Q5_random_number_generator.png
    * Q6_methods.png
* README.md (must display all pngs)

#### Assignment Screenshots:

| *Screenshot of A3 ERD*: | *Screenshot of Customer Table* | *Screenshot of Pet Table* | *Screenshot of Petstore Table* |
|:-----------------------:|:------------------------------:|:-------------------------:|:------------------------------:|
| ![ERD Screenshot](img/a3_ERD.png "ERD based up A3 requirements") | ![Customer Table Screenshot](img/cus_table.png "ERD Customer Table") | ![Pet Table Screenshot](img/pet_table.png "ERD Pet Table") | ![Petstore Screenshot](img/pts_table.png "ERD Petstore Table") |

| *Screenshot of My Event UI 1*: | *Screenshot of My Event UI 2* | *Screenshot of My Event UI 3* | *Screenshot My Event UI 4* |
|:-----------------------:|:------------------------------:|:-------------------------:|:------------------------------:|
| ![UI 1 Screenshot](img/event_ui_1.png "My Event UI 1") | ![UI 2 Screenshot](img/event_ui_2.png "My Event UI 2") | ![UI 3 Screenshot](img/event_ui_3.png "My Event UI 3") | ![UI 4 Screenshot](img/event_ui_4.png "My Event UI 4") |

[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")
[A3 SQL File](docs/a3.sql "A3 SQL Script")

| *Screenshot of Skillset 4: Decision Structures*: | *Screenshot of Skillset 5: PseudoRandom Integer Generator*: | *Screenshot of Skillset 6: Methods*: |
|:-------------------------------------------:|:-------------------------------------------:|:------------------------------------------------:|
[![Q4_decision_structure Screenshot](img/Q4_decision_structure.png)](img/Q4_decision_structure.png) | [![Q5_random_number_generator Screenshot](img/Q5_random_number_generator.png)](img/Q5_random_number_generator.png) | [![Q6_methods Screenshot](img/Q6_methods.png)](img/Q6_methods.png)
