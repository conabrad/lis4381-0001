**NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web App Dev

## Conner Bradley

### Assignment 1 Requirements:

*Three Parts*

1. Distributed Version Control System with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Ch. 1-2)

#### README.md file should include the following items:

* Screenshot of AMPPS installation 
* Screenshot of running java Hello
* Screenshot of Android Studio - My First App
* Git commands with short descriptions
* BitBucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstation locations and myteamquotes)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - one way to start a new project with Git
2. git status - shows status of working tree
3. git add - add files to staging area
4. git commit -m "message" - creates and saves snapshot of changes
5. git push - send local commits to the master branch of the remote repo
6. git pull - merges all changes in the remote repo with the local pwd
7. git remote -v - views all remote repos

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/conabrad/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/conabrad/myteamquotes/ "My Team Quotes Tutorial")